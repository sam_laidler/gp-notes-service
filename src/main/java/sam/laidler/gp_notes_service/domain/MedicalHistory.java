package sam.laidler.gp_notes_service.domain;

import java.util.List;

public class MedicalHistory {
	private String gpName;
	private List<GPNote> notes;
	
	public MedicalHistory(String gpName, List<GPNote> notes) {
		super();
		this.gpName = gpName;
		this.notes = notes;
	}
	
	public MedicalHistory() {}

	public String getGpName() {
		return gpName;
	}

	public void setGpName(String gpName) {
		this.gpName = gpName;
	}

	public List<GPNote> getNotes() {
		return notes;
	}

	public void setNotes(List<GPNote> notes) {
		this.notes = notes;
	}

	@Override
	public String toString() {
		return "MedicalHistory [gpName=" + gpName + ", notes=" + notes + "]";
	}
}
