package sam.laidler.gp_notes_service.domain;

import java.util.Date;

public class GPNote {
	private Date date;
	private String notes;

	public GPNote(Date date, String notes) {
		super();
		this.date = date;
		this.notes = notes;
	}
	
	public GPNote() {}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Override
	public String toString() {
		return "GPNote [Date=" + date + ", notes=" + notes + "]";
	}
}
