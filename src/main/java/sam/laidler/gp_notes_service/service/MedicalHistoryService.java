package sam.laidler.gp_notes_service.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebService;

import sam.laidler.gp_notes_service.domain.GPNote;
import sam.laidler.gp_notes_service.domain.MedicalHistory;

@WebService
public class MedicalHistoryService {
	private Map<Integer, MedicalHistory> dummyPatientHistory = new HashMap<Integer, MedicalHistory>();
	
	private List<GPNote> notes = new ArrayList<GPNote>();
	
	private GPNote cough = new GPNote(new Date(), "Slight cough");
	private GPNote lumbego = new GPNote(new Date(), "Back pain");
	private GPNote head = new GPNote(new Date(), "Severe headache");
	private GPNote eyes = new GPNote(new Date(), "Dry eyes");
	private GPNote ears = new GPNote(new Date(), "Loss of hearing");
	private GPNote jaw = new GPNote(new Date(), "Broken jaw");
	
	private String [] dummyGPs = {"The Manchester Clinic",
			"The Royal Berkshire Hospital",
			"London St Marys",
			"Birmingham General Admissions",
			"The Chievely Medical Practice",
			"Dagenham Medical Centre",
			"Oxford Medicentre"};
	
	{
		notes.add(cough);
		notes.add(lumbego);
		notes.add(head);
		notes.add(eyes);
		notes.add(ears);
		notes.add(jaw);
	}
	
	@WebMethod(exclude=true)
	private MedicalHistory populateDummyData() {
		// Choose a GP at random
		String gp = dummyGPs[(int) (Math.random()*dummyGPs.length)];
		
		// Remove a few notes at random, leaving at least 1
		ArrayList<GPNote> dummyNotesCopy = new ArrayList<GPNote>();
		dummyNotesCopy.addAll(this.notes);

		int numberOfNotes = (int) (Math.random()*this.notes.size());
		for (int i = 0; i < numberOfNotes && dummyNotesCopy.size() > 0; i++) {
			int index = (int) (Math.random()*dummyNotesCopy.size());
			GPNote note = dummyNotesCopy.get(index);
			dummyNotesCopy.remove(note);
		}
		return new MedicalHistory(gp, dummyNotesCopy);
	}
	
	public MedicalHistory getNotes(int nhsNumber) {
		MedicalHistory patientHistory = dummyPatientHistory.get(nhsNumber);
		
		if (patientHistory != null) {
			return patientHistory;
		}
		
		patientHistory = populateDummyData();
		dummyPatientHistory.put(nhsNumber, patientHistory);
		
		return patientHistory;
	}
}