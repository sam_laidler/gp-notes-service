@echo off
set /p appName="Enter app name (default=gp-notes-service): " || set "appName=gp-notes-service"
cp ..\..\..\target\myapp.war .
docker build --tag=%appName% .
docker run -it %appName%